/*      TFT Pong

 This example for the Arduino screen reads the values
 of 2 potentiometers to move a rectangular platform
 on the x and y axes. The platform can intersect
 with a ball causing it to bounce.

 This example code is in the public domain.

 Created by Tom Igoe December 2012
 Modified 15 April 2013 by Scott Fitzgerald
 Modified Jan 15, 2017 for use with Adafruit 2.2 TFT Screen
 Requires:
 https://github.com/adafruit/Adafruit-GFX-Library
 https://github.com/adafruit/Adafruit_ILI9340
 
 http://arduino.cc/en/Tutorial/TFTPong

 */

#include "Adafruit_ILI9340.h"
#include "Adafruit_GFX.h"

// pin definition for the Uno
#define cs 10
#define dc 9
#define rst 8

Adafruit_ILI9340 TFTscreen = Adafruit_ILI9340(cs, dc, rst);

// variables for the position of the ball and paddle
int paddle1X = 0;
int paddle1Y = 0;
int oldPaddle1X, oldPaddle1Y;
int paddle2X = 0;
int paddle2Y = 320-10/2;
int oldPaddle2X, oldPaddle2Y;
int ballDirectionX = 1;
int ballDirectionY = 1;

int ballSpeed = 10; // lower numbers are faster

int ballX, ballY, oldBallX, oldBallY;

void setup() {
    // initialize the display
    TFTscreen.begin();
   // TFTscreen.setRotation(45);
    // black background
    TFTscreen.fillScreen(ILI9340_BLACK);
}

void loop() {
// save the width and height of the screen
    int myWidth = 320*2;
    int myHeight = 240;

// map the paddle's location to the position of the potentiometers
    paddle1X = map(analogRead(A0), 1024, -1024, 0, myWidth) - 20 / 2;
    paddle1Y == - 5 / 2;
    paddle2X = map(analogRead(A1), 1024, -1024, 0, myWidth) - 20 / 2;
    paddle2Y == 240 - 5 / 2;

// set the fill color to black and erase the previous
// position of the paddle if different from present


    if (oldPaddle1X != paddle1X || oldPaddle1Y != paddle1Y) {
        TFTscreen.fillRect(oldPaddle1X, oldPaddle1Y, 20, 5,ILI9340_BLACK);
    }
    if (oldPaddle2X != paddle2X || oldPaddle2Y != paddle2Y) {
        TFTscreen.fillRect(oldPaddle2X, oldPaddle2Y, 20, 5,ILI9340_BLACK);
    }

// draw the paddle on screen, save the current position
// as the previous.

    TFTscreen.fillRect(paddle2X, paddle2Y, 20, 5,ILI9340_WHITE);
    oldPaddle2X = paddle2X;
    oldPaddle2Y = paddle2Y;

    TFTscreen.fillRect(paddle1X, paddle1Y, 20, 5,ILI9340_WHITE);
    oldPaddle1X = paddle1X;
    oldPaddle1Y = paddle1Y;

// update the ball's position and draw it on screen
    if (millis() % ballSpeed < 2) {
        moveBall();
    }
}
// reset the game when the ball drops,give the ball random direction and position
void reset()
{
    ballX = 50;
    ballY = 50;
    ballDirectionX = random(-5,5);
    ballDirectionY = 1;
//if ( ballDirectionX == 0 ) {
// ballDirectionX == -5;
// }
    /*else{
    ballDirectionX = 1;
    }
    ballDirectionY = random(0,1);
    if ( ballDirectionY == 0 ) {
    ballDirectionY = -1;
    }
    else
    {
    ballDirectionY = 1;
    }*/
    ballSpeed = 20;
    delay(1000);
}

// this function determines the ball's position on screen
void moveBall() {
// if the ball goes offscreen, reset the game
    if (ballX > 240 || ballX < 0) {
        ballDirectionX = -ballDirectionX;
    }
    if (ballY > 320 || ballY < 0) {
        reset();
    }
// check if the ball and the paddle occupy the same space on screen
    if (inPaddle(ballX, ballY, paddle1X, paddle1Y, 20, 5)) {
        ballDirectionX = random(-5,5);
        ballDirectionY = -ballDirectionY;
    }
    if (inPaddle(ballX, ballY, paddle2X, paddle2Y, 20, 5)) {
        ballDirectionX = random(-5,5);
        ballDirectionY = -ballDirectionY;
    }
// update the ball's position
    ballX += ballDirectionX;
    ballY += ballDirectionY;

// erase the ball's previous position
    if (oldBallX != ballX || oldBallY != ballY) {
        TFTscreen.fillRect(oldBallX, oldBallY, 5, 5,ILI9340_BLACK);
    }


// draw the ball's current position
    TFTscreen.fillRect(ballX, ballY, 5, 5,ILI9340_WHITE);

    oldBallX = ballX;
    oldBallY = ballY;
}


// this function checks the position of the ball
// to see if it intersects with the paddle
boolean inPaddle(int x, int y, int rectX, int rectY, int rectWidth, int rectHeight) {
    boolean result = false;
    if ((x >= rectX && x <= (rectX + rectWidth)) &&
            (y >= rectY && y <= (rectY + rectHeight))) {
        result = true;
    }
    return result;
}
