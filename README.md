# README #

This is a modification of the pong code to allow it to work with the Adafruit 2.2" TFT Screen.

For Wiring, follow:
https://llai2blog.wordpress.com/2015/05/03/50/

Requires:
https://github.com/adafruit/Adafruit-GFX-Library
https://github.com/adafruit/Adafruit_ILI9340

Original:
http://arduino.cc/en/Tutorial/TFTPong

Created by Tom Igoe December 2012
Modified 15 April 2013 by Scott Fitzgerald
Modified Jan 15, 2017 for use with Adafruit 2.2 TFT Screen by Adrian Chan